﻿using System;

namespace trpo_lab6_lib
{
    public class Class1
    {
        public double n; //Количество вещества (моль)
        static double R = 8.31; //R - газовая постоянная
        public double T; //T - температура (const) (К)
        public double V1; //V1 - начальный объём
        public double V2; //V2 - конечный объём

        public int i; //i - число степеней свободы (3,5,6)
        public double T1; // T1 - начальная температура
        public double T2; //T2 - конечная температура
        public double P; //P - давление(const)


        public Class1(double n, double T, double V1, double V2)
        {
            this.n = n;
            this.T = T;
            this.V1 = V1;
            this.V2 = V2;
            if ((n <= 0) || (T < 273) || (V1 <= 0) || (V2 <= 0))
                throw new ArgumentException();
        }
        public Class1(double n, int i, double T1, double T2)
        {
            this.n = n;
            this.i = i;
            this.T1 = T1;
            this.T2 = T2;
            if ((n <= 0) || ((i != 3) & (i != 5) & (i != 6)) || (T1 < 273) || (T2 < 273))
                throw new ArgumentException();
        }

        public Class1(double n, int i, double T1, double T2, double V1, double V2, double P)
        {
            this.n = n;
            this.i = i;
            this.T1 = T1;
            this.T2 = T2;
            this.V1 = V1;
            this.V2 = V2;
            this.P = P;
            if ((n <= 0) || ((i != 3) & (i != 5) & (i != 6)) || (T1 < 273) || (T2 < 273) || (V1 <= 0) || (V2 <= 0) || (P <= 0))
                throw new ArgumentException();
        }

        public double Heat_Temperature() //Изотермический процесс
        {
            return (this.n * R * this.T * Math.Log(this.V2/this.V1));
        }
        public double Heat_Volume() //Изохорный процесс
        {
            return ((Convert.ToDouble(this.i) / 2) * this.n * R * (this.T2 - this.T1));
        }
        public double Heat_Pressure() //Изобарный процесс
        {
            double A = (Convert.ToDouble(this.i) / 2) * this.n * R * (this.T2 - this.T1);
            double U = this.P * (this.V2 - this.V1);
            return (A + U);
        }
    }
}
