﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trpo_lab6_lib
{
    public class Class2
    {
        //Определить КПД нагревателя, расходующего m г вещества на нагревание V л воды на ΔT К
        public double m; //масса керосина 
        public double V; //объём воды
        public double T; // дельта Т
        public double q; //T2 - Удельная теплота сгорания вещества (МДж/кг)

        const double c = 4200; // удельная теплоемкость воды (Дж/(кг·К))
        const double p = 1000; // плотность воды (кг/м3)


        public Class2(double m, double V, double T, double q)
        {
            this.m = m;
            this.V = V;
            this.T = T;
            this.q = q;
            if ((m <= 0) || (V <= 0) || (T < 0) || (q <= 0))
                throw new ArgumentException();
        }
        public double Efficiency() //КПД нагревателя
        {
            double Ap = c * p * this.V * 0.001 * this.T;
            double Az = this.q * 0.001 * this.m * Math.Pow(10,6);
            return (Ap / Az);
        }
    }
}
