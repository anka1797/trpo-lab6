using NUnit.Framework;
using trpo_lab6_lib;
using System;

namespace trpo_ab6_tests
{
    public class Tests2
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestEfficiency() //��� �����������
        {
            const double m = 80;
            const double V = 3;
            const double T = 90;
            const double q = 43; //�������� ������� �������� �������� (���/��)

            const double expected = 0.3296511628;
            double result = new Class2(m, V, T, q).Efficiency();
            Assert.AreEqual(expected, result, 0.00001);
        }


        [Test]
        public void TestArgument() //��� �����������
        {
            const double m = -3;
            const double V = 270;
            const double T = -5;
            const double q = -1;
            Assert.Throws<ArgumentException>(() => new Class2(m, V, T, q).Efficiency());
        }
        
    }
}