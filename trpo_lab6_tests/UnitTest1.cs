using NUnit.Framework;
using trpo_lab6_lib;
using System;

namespace trpo_ab6_tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestHeat_Temperature() //�������������� �������
        {
            const double N = 3;
            const double T = 350;
            const double V1 = 0.2;
            const double V2 = 0.6;
            const double expected = 9585.941525;
            double result = new Class1(N, T, V1, V2).Heat_Temperature();
            Assert.AreEqual(expected, result, 0.00001);
        }

        [Test]
        public void TestHeat_Volume() //��������� �������
        {
            const double N = 3;
            const int i = 5;
            const double T1 = 273;
            const double T2 = 373;

            const double expected = 6232.5;
            double result = new Class1(N, i, T1, T2).Heat_Volume();
            Assert.AreEqual(expected, result, 0.00001);
        }
        [Test]
        public void TestHeat_Pressure() //��������� �������
        {
            const double N = 3;
            const double V1 = 0.2;
            const double V2 = 0.6;
            const int i = 3;
            const double T1 = 273;
            const double T2 = 373;
            const double P = 100;

            const double expected = 3779.5;
            double result = new Class1(N, i, T1, T2, V1, V2, P).Heat_Pressure();
            Assert.AreEqual(expected, result, 0.00001);
        }
        [Test]
        public void TestArgument1() //�������������� �������
        {
            const double N = -3;
            const double T = -2;
            const double V1 = -5;
            const double V2 = -1;
            Assert.Throws<ArgumentException>(() => new Class1(N, T, V1, V2).Heat_Temperature());
        }
        [Test]
        public void TestArgument2() //��������� �������
        {
            const double N = -3;
            const int i = -3;
            const double T1 = -273;
            const double T2 = -373;
            Assert.Throws<ArgumentException>(() => new Class1(N, i, T1, T2).Heat_Volume());
        }
        [Test]
        public void TestArgument3() //��������� �������
        {
            const double N = -3;
            const double V1 = -0.2;
            const double V2 = -0.6;
            const int i = -3;
            const double T1 =-273;
            const double T2 = -373;
            const double P = -100;
            Assert.Throws<ArgumentException>(() => new Class1(N, i, T1, T2, V1, V2, P).Heat_Pressure());
        }
    }
}
