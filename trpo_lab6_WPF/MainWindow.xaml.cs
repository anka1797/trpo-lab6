﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using System.ComponentModel;
using trpo_lab6_lib;
using System.Diagnostics;
using Word = Microsoft.Office.Interop.Word;

namespace trpo_lab6_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
        private void Help_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(@"Справка.pdf") { UseShellExecute = true });


        }

        private void report_Click(object sender, RoutedEventArgs e)
        {
            var text = "Изотермический процесс: \n" + label1.Content + "\n" + label_N1.Content + " " + textbox1n.Text + "\n" + label_T1.Content + " " + textbox1T.Text + "\n" + label_V11.Content + " " + textbox1V1.Text + "\n" + label_V21.Content + " " + textbox1V2.Text + "\n" + label_answ1.Content + "\n" + label_Q1.Content + " " + textbox1Res.Text; 
            Word.Application app = new Word.Application();
            Word.Document doc = app.Documents.Add();
            doc.Paragraphs[1].Range.Text = text;
            app.Visible = true;
        }
        private void report2_Click(object sender, RoutedEventArgs e)
        {
            var text = "Изохорный процесс: \n" + label2.Content + "\n" + label_N2.Content + " " + textbox2n.Text + "\n" + label_I2.Content + " " + textbox2i.Text + "\n" + label_T12.Content + " " + textbox2T1.Text + "\n" + label_T22.Content + " " + textbox2T2.Text + "\n" + label_answ2.Content + "\n" + label_Q2.Content + " " + textbox2Res.Text;
            Word.Application app = new Word.Application();
            Word.Document doc = app.Documents.Add();
            doc.Paragraphs[1].Range.Text = text;
            app.Visible = true;
        }
        private void report3_Click(object sender, RoutedEventArgs e)
        {
            var text = "Изобарный процесс: \n" + label3.Content + "\n" + label_N3.Content + " " + textbox3n.Text + "\n" + label_I3.Content + " " + textbox3i.Text + "\n" + label_T13.Content + " " + textbox3T1.Text + "\n" + label_T23.Content + " " + textbox3T2.Text + "\n" + label_V13.Content + " " + textbox3V1.Text + "\n" + label_V23.Content + " " + textbox3V2.Text + "\n" + label_P3.Content + " " + textbox3P.Text + "\n" + label_answ3.Content + "\n" + label_Q3.Content + " " + textbox3Res.Text;
            Word.Application app = new Word.Application();
            Word.Document doc = app.Documents.Add();
            doc.Paragraphs[1].Range.Text = text;
            app.Visible = true;
        }
        private void report4_Click(object sender, RoutedEventArgs e)
        {
            var text = label4.Content + "\n" + label5.Content + "\n" + label_M4.Content + " " + textbox4m.Text + "\n" + label_V14.Content + " " + textbox4V1.Text + "\n" + label_T4.Content + " " + textbox4T.Text + "\n" + label_Q4.Content + " " + textbox4q.Text + "\n" + label_C4.Content + "\n" + label_P4.Content + "\n" + label_answ4.Content + "\n" + label_n4.Content + " " + textbox4Res.Text;
            Word.Application app = new Word.Application();
            Word.Document doc = app.Documents.Add();
            doc.Paragraphs[1].Range.Text = text;
            app.Visible = true;
        }
    }
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private double _N = 1;
        public double N
        {
            get { return _N; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _N = value;
                if (_N > 0)
                {
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result1)));
                }

            }
        }
        private double _T = 273;
        public double T
        {
            get { return _T; }
            set
            {
                if (value < 273)
                {
                    MessageBox.Show("Значение должно быть больше 273");
                    return;
                }
                _T = value;
                if (_T >= 273)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result1)));
            }
        }
        private double _V1 = 1;
        public double V1
        {
            get { return _V1; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _V1 = value;
                if (_V1 > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result1)));
            }
        }
        private double _V2 = 1;
        public double V2
        {
            get { return _V2; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _V2 = value;
                if (_V2 > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result1)));
            }
        }
        private double _N2 = 1;
        public double N2
        {
            get { return _N2; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _N2 = value;
                if (_N2 > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result2)));
            }
        }
        private int _I = 3;
        public int I
        {
            get { return _I; }
            set
            {
                if (value != 3)
                {
                    if (value != 5)
                    {
                        if (value != 6)
                        {
                            MessageBox.Show("Значение должно быть равно 3,5 или 6");
                            return;
                        }
                    }
                }

                _I = value;
                if (_I > -1)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result2)));
            }
        }

        private double _T1 = 273;
        public double T1
        {
            get { return _T1; }
            set
            {
                if (value < 273)
                {
                    MessageBox.Show("Значение должно быть больше 273");
                    return;
                }
                _T1 = value;
                if (_T1 >= 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result2)));
            }
        }
        private double _T2 = 273;
        public double T2
        {
            get { return _T2; }
            set
            {
                if (value < 273)
                {
                    MessageBox.Show("Значение должно быть больше 273");
                    return;
                }
                _T2 = value;
                if (_T2 >= 273)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result2)));
            }
        }
        private double _N3 = 1;
        public double N3
        {
            get { return _N3; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _N3 = value;
                if (_N3 > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }
        private int _I3 = 3;
        public int I3
        {
            get { return _I3; }
            set
            {
                if (value != 3)
                {
                    if (value != 5)
                    {
                        if (value != 6)
                        {
                            MessageBox.Show("Значение должно быть равно 3,5 или 6");
                            return;
                        }
                    }
                }

                _I3 = value;
                if (_I3 > -1)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }
        private double _T1_3 = 273;
        public double T1_3
        {
            get { return _T1_3; }
            set
            {
                if (value < 273)
                {
                    MessageBox.Show("Значение должно быть больше 273");
                    return;
                }
                _T1_3 = value;
                if (_T1_3 >= 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }
        private double _T2_3 = 273;
        public double T2_3
        {
            get { return _T2_3; }
            set
            {
                if (value < 273)
                {
                    MessageBox.Show("Значение должно быть больше 273");
                    return;
                }
                _T2_3 = value;
                if (_T2_3 >= 273)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }
        private double _V1_3 = 1;
        public double V1_3
        {
            get { return _V1_3; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _V1_3 = value;
                if (_V1_3 > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }
        private double _V2_3 = 1;
        public double V2_3
        {
            get { return _V2_3; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _V2_3 = value;
                if (_V2_3 > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }
        private double _P = 1;
        public double P
        {
            get { return _P; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _P = value;
                if (_P > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }


        public double Result1
        {
            get { return new Class1(N, T, V1, V2).Heat_Temperature(); }
        }

        public double Result2
        {
            get { return new Class1(N2, I, T1, T2).Heat_Volume(); }
        }

        public double Result3
        {
            get { return new Class1(N3, I3, T1_3, T2_3, V1_3, V2_3, P).Heat_Pressure(); }
        }


        private double _M = 1;
        public double M
        {
            get { return _M; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше 0");
                    return;
                }
                _M = value;
                if (_M > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }

        private double _V_4 = 1;
        public double V_4
        {
            get { return _V_4; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _V_4 = value;
                if (_V_4 > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result3)));
            }
        }

        private double _T_4 = 1;
        public double T_4
        {
            get { return _T_4; }
            set
            {
                if (value < 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _T_4 = value;
                if (_T_4 >= 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result4)));
            }
        }

        private double _Q = 1;
        public double Q
        {
            get { return _Q; }
            set
            {
                if (value <= 0)
                {
                    MessageBox.Show("Значение должно быть больше нуля");
                    return;
                }
                _Q = value;
                if (_Q > 0)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result4)));
            }
        }
        public double Result4
        {
            get { return new Class2(M, V_4, T_4, Q).Efficiency(); }
        }

    }

}
